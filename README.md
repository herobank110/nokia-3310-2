## My game

Live demo:
<https://www.airconsole.com/#https://herobank110.gitlab.io/nokia-3310-2>

Preview Pages:
[https://herobank110.gitlab.io/nokia-3310-2/](https://herobank110.gitlab.io/nokia-3310-2/)



## Resources Used
* Three.js game engine:
[https://threejs.org/](https://threejs.org/)

* Dither shader effect:
[https://github.com/hughsk/glsl-dither](https://github.com/hughsk/glsl-dither)

* Depth rendering shader, adapted from:
[https://github.com/mrdoob/three.js/blob/master/examples/webgl_depth_texture.html](https://github.com/mrdoob/three.js/blob/master/examples/webgl_depth_texture.html)

* Font, adapted from:
[https://ff.static.1001fonts.net/s/u/subway-ticker.regular.ttf](https://ff.static.1001fonts.net/s/u/subway-ticker.regular.ttf)
